@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Categories</div>

                <div class="panel-body">     
              
                    @can('categories.edit')
                        <p class="text-right">
                            <a href="{{ route('categories.create') }}" class="btn btn-primary">
                                Crear
                            </a>
                        </p>
                    @endcan

                    <table class="table table-striped">
                        @foreach($categories as $category)
                        <tbody>
                            <tr>
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->name }}</td>
                                <td>{{ $category->description }}</td>

                                @can('categories.edit')
                                <td>
                                    <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-sm btn-default">
                                        Editar
                                    </a>
                                </td>
                                @endcan

                                @can('categories.show')
                                <td>
                                    <a href="{{ route('categories.show', $category) }}" class="btn btn-sm btn-default">
                                        Ver
                                    </a>
                                </td>
                                @endcan

                                @can('categories.destroy')
                                <td>
                                    <form action="{{ route('categories.destroy', $category->id) }}" method="POST">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-sm btn-danger">
                                            Eliminar
                                        </button>
                                    </form>
                                </td>
                                @endcan

                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                    <div class="text-center">
                        {!! $categories->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection