@extends('layouts.app')

@section('title', '| Create New Category')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

        <h1>Create New Category</h1>
        <hr>

   		 {{-- Using the Laravel HTML Form Collective to create our form --}}
        {{ Form::open(array('route' => 'categories.store')) }}

        <div class="form-group">
        	{{ Form::label('value', 'Value') }}
            {{ Form::text('value', null, array('class' => 'form-control')) }}
            
            {{ Form::label('name', 'Name') }}
            {{ Form::text('name', null, array('class' => 'form-control')) }}
            <br>

            {{ Form::label('description', 'Description') }}
            {{ Form::textarea('description', null, array('class' => 'form-control')) }}
            <br>

            {{ Form::submit('Create Post', array('class' => 'btn btn-success btn-lg btn-block')) }}
            {{ Form::close() }}
        </div>
        </div>
    </div>

@endsection