@extends('layouts.app')

@section('title', '| Edit Category')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

        <h1>Edit {{ $category->name }}</h1>
        <hr>
		<!-- if there are creation errors, they will show here -->
		<!-- {{-- HTML::ul($errors->all()) --}}-->
		
		
		{{ Form::model($category, array('route' => array('categories.update', $category->id), 'method' => 'PUT')) }}
        
          <div class="form-group">
        	{{ Form::label('value', 'Value') }}
            {{ Form::text('value', null, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', null, array('class' => 'form-control')) }}
            </div>
        
            <div class="form-group">
                {{ Form::label('description', 'Description') }}
            {{ Form::textarea('description', null, array('class' => 'form-control')) }}
            </div>
        
        
            {{ Form::submit('Edit the Category!', array('class' => 'btn btn-primary btn-lg btn-block')) }}
        
        {{ Form::close() }}
        
   		 {{-- Using the Laravel HTML Form Collective to create our form --}}
        {{ Form::open(array('route' => 'categories.store')) }}

        </div>
    </div>
    

@endsection