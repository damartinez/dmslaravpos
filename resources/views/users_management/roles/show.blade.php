@extends('layouts.app')

@section('content')
<div class="container">

    <h1>{{ $category->name }}</h1>
    <hr>
    <p class="lead">{{ $category->description }} </p>
    <hr>
    {!! Form::open(['method' => 'DELETE', 'route' => ['categories.destroy', $category->id] ]) !!}
    <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
    @can('categories.edit')
    <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-info" role="button">Edit</a>
    @endcan
    @can('categories.destroy')
    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
    @endcan
    {!! Form::close() !!}

</div>
@endsection