<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    /* The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'value', 'name', 'description', 'created_by', 'updated_by', 'category_parent_id', 'is_default',
    ];
}
