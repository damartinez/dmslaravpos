<?php

namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CategoriesController extends Controller
{
    
    private $route_view = 'material_management.categories.';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categories::orderBy('id','desc')->paginate(5);
        return view( $this->route_view . 'index', compact('categories') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( $this->route_view . 'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $created_by = auth()->user();
        // '', '', , 'created_by', 'updated_by', 'category_parent_id', 'is_default'
       
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'value'=>'required|max:60',
            'name' =>'required|max:60',
            'description' => 'max:100',
        );
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
            return Redirect::to('categories/create')
            ->withErrors($validator)
//             ->withInput(Input::except('password'))
            ->withInput()
            ;
        } else {
            // store
            $category = new Categories();
            $category->value       = Input::get('value');
            $category->name      = Input::get('name');
            $category->description = Input::get('description');
            $category->created_by = $created_by->id;
            $category->updated_by = $created_by->id;
            $category->save();
            
            // redirect
            Session::flash('message', 'Successfully created category! ' .  $category->name );
            return Redirect::to('categories');
        }
        
       // $category = Categories::create($request->only('value', 'name', 'description'));
        /*//Display a successful message upon save
        return redirect()->route('categories.index')
        ->with('flash_message', 'Category,
             '. $category->name.' created');*/
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function show($id, Categories $category)
    {
        /*var_dump($id);
        var_dump($category);exit;*/
        // get the nerd
        $category = Categories::find($id);
        
        // show the view and pass the nerd to it
        /*return View::make('nerds.show')
        ->with('nerd', $nerd);
        */
        return view ( $this->route_view . 'show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Categories $category)
    {
        $category = Categories::find($id);
        return view ( $this->route_view . 'edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categories $category, $id)
    {
        $updated_by = auth()->user();
        // '', '', , 'created_by', 'updated_by', 'category_parent_id', 'is_default'
        
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'value'=>'required|max:60',
            'name' =>'required|max:60',
            'description' => 'max:100',
        );
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
            return Redirect::to('categories/' . $id . '/edit')
            ->withErrors($validator)
            //             ->withInput(Input::except('password'))
            ->withInput()
            ;
        } else {
            // store
            $category = Categories::find($id);
            $category->value       = Input::get('value');
            $category->name      = Input::get('name');
            $category->description = Input::get('description');
            $category->updated_by = $updated_by->id;
            $category->save();
            
            // redirect
            Session::flash('message', 'Successfully updated category! ' .  $category->name );
            return Redirect::to('categories');
        }
        
        //Display a successful message upon save
      /*  return redirect()->route('categories.index')
        ->with('flash_message', 'Category,
             '. $categories->name.' updated');*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categories $categories, $id)
    {
        $category = Categories::find($id);
        $category->delete();
        
        return redirect()->route('categories.index')
        ->with('flash_message',
            'Category successfully deleted');
    }
}
