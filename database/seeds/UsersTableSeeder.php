<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Dixon Martinez',
            'email' => 'dixon22martinez@gmail.com',
            'password' => bcrypt( 'dmartinez' ),
        ]);
        
        factory(User::class, 10)->create();
    }
}
