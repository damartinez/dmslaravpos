<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Permission list
        // Products
        Permission::create(['name' => 'products.index']);
        Permission::create(['name' => 'products.edit']);
        Permission::create(['name' => 'products.show']);
        Permission::create(['name' => 'products.create']);
        Permission::create(['name' => 'products.destroy']);
        
        // Categories
        Permission::create(['name' => 'categories.index']);
        Permission::create(['name' => 'categories.edit']);
        Permission::create(['name' => 'categories.show']);
        Permission::create(['name' => 'categories.create']);
        Permission::create(['name' => 'categories.destroy']);
        
        // Users
        Permission::create(['name' => 'users.index']);
        Permission::create(['name' => 'users.edit']);
        Permission::create(['name' => 'users.show']);
        Permission::create(['name' => 'users.create']);
        Permission::create(['name' => 'users.destroy']);
        
        // Permissions
        Permission::create(['name' => 'permissions.index']);
        Permission::create(['name' => 'permissions.edit']);
        Permission::create(['name' => 'permissions.show']);
        Permission::create(['name' => 'permissions.create']);
        Permission::create(['name' => 'permissions.destroy']);
        
        // Roles
        Permission::create(['name' => 'roles.index']);
        Permission::create(['name' => 'roles.edit']);
        Permission::create(['name' => 'roles.show']);
        Permission::create(['name' => 'roles.create']);
        Permission::create(['name' => 'roles.destroy']);
        //Admin
        $admin = Role::create(['name' => 'Admin']);
        /*
        $admin->givePermissionTo([
            'products.index',
            'products.edit',
            'products.show',
            'products.create',
            'products.destroy'
        ]);
        */
        //$admin->givePermissionTo('products.index');
        $admin->givePermissionTo(Permission::all());
        
        //Guest
        $guest = Role::create(['name' => 'Guest']);
        
        $guest->givePermissionTo([
            'products.index',
            'products.show'
        ]);
        
        //User Admin
        $user = User::find(1); //Italo Morales
        $user->assignRole('Admin');
        /*
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'product-list',
            'product-create',
            'product-edit',
            'product-delete'
        ];
        
        
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }*/
    }
}
