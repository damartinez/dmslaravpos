<?php

use Faker\Generator as Faker;

$factory->define(App\Categories::class, function (Faker $faker) {
    return [
        'value' =>  $faker->sentence, 
        'name' => $faker->sentence , 
        'description' => $faker->text(500), 
        'created_by' => 1, 
        'updated_by' => 1, 
        'category_parent_id' => null, 
        'is_default' => false,
    ];
});
