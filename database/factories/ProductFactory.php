<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'value' => $faker->sentence,
        'name' => $faker->sentence,
        'description' => $faker->text(500),
    ];
});
