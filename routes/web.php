<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::middleware(['auth'])->group(function () {
    //  Products
    Route::post('products/store', 'ProductController@store')->name('products.store')
    ->middleware('permission:products.create');
    Route::get('products', 'ProductController@index')->name('products.index')
    ->middleware('permission:products.index');
    Route::get('products/create', 'ProductController@create')->name('products.create')
    ->middleware('permission:products.create');
    Route::put('products/{role}', 'ProductController@update')->name('products.update')
    ->middleware('permission:products.edit');
    Route::get('products/{role}', 'ProductController@show')->name('products.show')
    ->middleware('permission:products.show');
    Route::delete('products/{role}', 'ProductController@destroy')->name('products.destroy')
    ->middleware('permission:products.destroy');
    Route::get('products/{role}/edit', 'ProductController@edit')->name('products.edit')
    ->middleware('permission:products.edit');
    //  Categories
    Route::post('categories/store', 'CategoriesController@store')->name('categories.store')
    ->middleware('permission:categories.create');
    Route::get('categories', 'CategoriesController@index')->name('categories.index')
    ->middleware('permission:categories.index');
    Route::get('categories/create', 'CategoriesController@create')->name('categories.create')
    ->middleware('permission:categories.create');
    Route::put('categories/{role}', 'CategoriesController@update')->name('categories.update')
    ->middleware('permission:categories.edit');
    Route::get('categories/{role}/edit', 'CategoriesController@edit')->name('categories.edit')
    ->middleware('permission:categories.edit');
    Route::get('categories/{role}', 'CategoriesController@show')->name('categories.show')
    ->middleware('permission:categories.show');
    Route::delete('categories/{role}', 'CategoriesController@destroy')->name('categories.destroy')
    ->middleware('permission:categories.destroy');
    
    //  Users
    Route::post('users/store', 'UserController@store')->name('users.store')
    ->middleware('permission:users.create');
    Route::get('users', 'UserController@index')->name('users.index')
    ->middleware('permission:users.index');
    Route::get('users/create', 'UserController@create')->name('users.create')
    ->middleware('permission:users.create');
    Route::put('users/{role}', 'UserController@update')->name('users.update')
    ->middleware('permission:users.edit');
    Route::get('users/{role}/edit', 'UserController@edit')->name('users.edit')
    ->middleware('permission:users.edit');
    Route::get('users/{role}', 'UserController@show')->name('users.show')
    ->middleware('permission:users.show');
    Route::delete('users/{role}', 'UserController@destroy')->name('users.destroy')
    ->middleware('permission:users.destroy');
    
    //  Roles
    Route::post('roles/store', 'RolesController@store')->name('roles.store')
    ->middleware('permission:roles.create');
    Route::get('roles', 'RolesController@index')->name('roles.index')
    ->middleware('permission:roles.index');
    Route::get('roles/create', 'RolesController@create')->name('roles.create')
    ->middleware('permission:roles.create');
    Route::put('roles/{role}', 'RolesController@update')->name('roles.update')
    ->middleware('permission:roles.edit');
    Route::get('roles/{role}/edit', 'RolesController@edit')->name('roles.edit')
    ->middleware('permission:roles.edit');
    Route::get('roles/{role}', 'RolesController@show')->name('roles.show')
    ->middleware('permission:roles.show');
    Route::delete('roles/{role}', 'RolesController@destroy')->name('roles.destroy')
    ->middleware('permission:roles.destroy');
    
    //  Permissions
    Route::post('permissions/store', 'PermissionsController@store')->name('permissions.store')
    ->middleware('permission:permissions.create');
    Route::get('permissions', 'PermissionsController@index')->name('permissions.index')
    ->middleware('permission:permissions.index');
    Route::get('permissions/create', 'PermissionsController@create')->name('permissions.create')
    ->middleware('permission:permissions.create');
    Route::put('permissions/{role}', 'PermissionsController@update')->name('permissions.update')
    ->middleware('permission:permissions.edit');
    Route::get('permissions/{role}/edit', 'PermissionsController@edit')->name('permissions.edit')
    ->middleware('permission:permissions.edit');
    Route::get('permissions/{role}', 'PermissionsController@show')->name('permissions.show')
    ->middleware('permission:permissions.show');
    Route::delete('permissions/{role}', 'PermissionsController@destroy')->name('permissions.destroy')
    ->middleware('permission:permissions.destroy');
    
});